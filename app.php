<?php
/*function __autoload($class_name){
    require_once $class_name.'.php';
}
*/
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

####################################################
####################################################
###########  EXEMPLE client HTTP ###################
####################################################
####################################################

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000';
$token      = 'C0UsWlYxXrMx81TKN2Eq';
$client     = new HTTPClient($base_url, $token);
//$result = $client->query('/api/v1/alumnes');
//$result = $client->query('/api/v1/alumne', $params, 'POST');
//$result = $client->query('/api/v1/alumne/:id', $params, 'PUT');
//$result = $client->query('/api/v1/alumne/:id', [], 'DELETE');
$resultAlumnes = JSONParser::parseFile(__DIR__.'/info/alumnes.json');
foreach ($resultAlumnes['data'] as $key=> $alumne) {
    $params = [
        'nom' => $alumne->nom,
        'cognoms' => $alumne->cognoms,
        'mail' => $alumne->email,        
    ];
    
    $result = $client->query('/api/v1/alumne', $params, 'POST');
}
$resultAssignatures = JSONParser::parseFile(__DIR__.'/info/assignatures.json');

foreach ($resultAssignatures['data'] as $key => $assignatura) {
    
    $params = [
        'nom' => $assignatura->nom,
        'professor' => $assignatura->professor              
    ];
    
    $result = $client->query('/api/v1/assignatura', $params, 'POST');
}
/*
# Si necessitem enviar paramatres a l'API, primer crearem un array amb els values
$params = [
    'nom' => 'Edu Gama',
    'cognoms' => 'Gama',
];

## Mitjançant el client instanciat anteriorment, podem realitzar crides a l'API amb varis metodes
## i també enviar parametres.
# $client->query(string $uri, array $params = [], string $method='GET')
# Aquesta petició sempre ens retornarà un array amb dos camps:
## status =  true / false 
## data = retorn de l'API



####################################################
####################################################
###########  EXEMPLE parser JSON ###################
####################################################
####################################################
/*$result = JSONParser::parseFile(__DIR__.'/alumnes.json');
var_dump($result['data']);
die();
# $result contindrà dos camps
## status =  true / false 
## data = array amb els valors del json o el codi d'error*/